import { RouterModule, Routes } from '@angular/router';

import { PagesComponent } from './pages/pages.component';

import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './login/register.component';
import { ProgressComponent } from './pages/progress/progress.component';
import { Graficas1Component } from './pages/graficas1/graficas1.component';
import { NopagefoundComponent } from './shared/nopagefound/nopagefound.component';
import { PromesasComponent } from './pages/promesas/promesas.component';

const appRoutes: Routes = [
    {
        path: '',
        component: PagesComponent ,
    children: [
        {path: 'dashboard', component: DashboardComponent},
        {path: 'progress', component: ProgressComponent},
        {path: 'graficas1', component: Graficas1Component},
        {path: 'promesas', component: PromesasComponent},
        {path: '', redirectTo: '/dashboard', pathMatch: 'full'}, // cualquier rita vacia la direcciona al dashb

    ]}, // cualquier ruta me lleva al pagesC
    {path: 'login', component: LoginComponent},
    {path: 'register', component: RegisterComponent},
    {path: '**', component: NopagefoundComponent} // cualquier otra ruta que no este definida acá , me mande al nopagefound
];
export const APP_ROUTES = RouterModule.forRoot(appRoutes, {useHash: true});
